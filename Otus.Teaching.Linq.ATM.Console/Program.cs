﻿using System;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;
using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WindowWidth = 100;
            System.Console.WindowHeight = 50;
            System.Console.OutputEncoding = System.Text.Encoding.UTF8;
            System.Console.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();

            atmManager.OutputUserData("lee", "222");
            atmManager.OutputUserAccountsData(2);
            atmManager.OutputUserAccountsDataWithHistory(5);
            atmManager.OutputHistoryWithUser(OperationType.InputCash);
            atmManager.OutputUsersWithMinSum(10000m, true);

            System.Console.WriteLine("\nЗавершение работы приложения-банкомата...");
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();
                
            return new ATMManager(accounts, users, history);
        }
    }
}