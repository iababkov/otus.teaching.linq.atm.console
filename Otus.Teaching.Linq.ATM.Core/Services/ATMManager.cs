﻿using System.Collections.Generic;
using Otus.Teaching.Linq.ATM.Core.Entities;
using System.Linq;
using System;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }
        
        public IEnumerable<User> Users { get; private set; }
        
        public IEnumerable<OperationsHistory> History { get; private set; }
        
        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        public void OutputUserData(string login, string password)
        {
            Console.WriteLine($"\n    Данные пользователя с логином {login}:");

            User user = Users.Where(user => user.Login == login && user.Password == password).FirstOrDefault();

            if (user != null)
            {
                Console.WriteLine($"Код пользователя: {user.Id}");
                Console.WriteLine($"Имя: {user.FirstName}");
                Console.WriteLine($"Фамилия: {user.SurName}");
                Console.WriteLine($"Отчество: {user.MiddleName}");
                Console.WriteLine($"Телефон: {user.Phone}");
                Console.WriteLine($"Паспорт: {user.PassportSeriesAndNumber}");
                Console.WriteLine($"Дата регистрации: {user.RegistrationDate}");
            }
            else
            {
                Console.WriteLine($"Логин и/или пароль указаны неверно!");
            }
        }

        public void OutputUserAccountsData(int userId)
        {
            Console.WriteLine($"\n    Счета пользователя {userId}:");

            var query = Accounts.Where(acc => acc.UserId == userId);

            foreach (Account acc in query)
            {
                Console.WriteLine($"Код счёта: {acc.Id}, Дата открытия: {acc.OpeningDate}, Баланс: {acc.CashAll}");
            }
        }

        public void OutputUserAccountsDataWithHistory(int userId)
        {
            Console.WriteLine($"\n    Счета пользователя {userId} с историей:");

            var query = Accounts.Where(acc => acc.UserId == userId)
                                .GroupJoin(History, acc => acc.Id, hist => hist.AccountId, (acc, hist) => new {Acc = acc, Hist = hist});

            foreach (var result in query)
            {
                Console.WriteLine($"Код счёта: {result.Acc.Id}, Дата открытия: {result.Acc.OpeningDate}, Баланс: {result.Acc.CashAll}");

                foreach (OperationsHistory hist in result.Hist)
                {
                    Console.WriteLine($"Код операции: {hist.Id}, Дата: {hist.OperationDate}, Тип: {hist.OperationType}, Сумма: {hist.CashSum}");
                }
            }
        }

        public void OutputHistoryWithUser(OperationType opType)
        {
            Console.WriteLine($"\n    {(opType == OperationType.InputCash ? "Приходные" : "Расходные")} операции:");

            var query = History.Where(hist => hist.OperationType == opType)
                                .Join(Accounts, hist => hist.AccountId, acc => acc.Id, (hist, acc) => new { Hist = hist, UserId = acc.UserId });

            foreach (var result in query)
            {
                Console.WriteLine($"Код операции: {result.Hist.Id}, Дата: {result.Hist.OperationDate}, Сумма: {result.Hist.CashSum}, Код счёта: {result.Hist.Id}, Код пользователя: {result.UserId}");
            }
        }

        public void OutputUsersWithMinSum(decimal minSum, bool minSumExcluded)
        {
            Console.WriteLine($"\n    Пользователи с суммами на счетах {(minSumExcluded ? "больше" : "от")} {minSum}:");

            var query = Accounts.Where(acc => (minSumExcluded ? acc.CashAll > minSum : acc.CashAll >= minSum))
                                .Join(Users, acc => acc.UserId, user => user.Id, (acc, user) => new { Users = user});

            foreach (var result in query)
            {
                Console.WriteLine($"Код пользователя: {result.Users.Id}, Имя: {result.Users.FirstName}, Фамилия: {result.Users.SurName}");
            }
        }
    }
}